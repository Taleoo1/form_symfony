<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121092345 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tmsg ADD date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE temail_tperson DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE temail_tperson ADD PRIMARY KEY (tperson_id, temail_id)');
        $this->addSql('ALTER TABLE temail_tperson RENAME INDEX idx_90993047db5ae27d TO IDX_B16856B0DB5AE27D');
        $this->addSql('ALTER TABLE temail_tperson RENAME INDEX idx_90993047865698d7 TO IDX_B16856B0865698D7');
        $this->addSql('ALTER TABLE tmsg DROP date');
    }
}
