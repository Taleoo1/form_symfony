<?php 


namespace App\Form\Type;

use App\Entity\TEmail;
use App\Entity\TMsg;
use App\Form\TMsgType;
use App\Form\TPersonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;




class StateForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
      /*
        Custom form builder to update the state of msgs in the result page
      */
      $builder->add('State', ChoiceType::class, [
        'choices' => [
            'A traiter' => 'A traiter',
            'Attente de réponse' => 'Attente',
            'RDV Pris' => 'RDV Pris',
            'Sans suite' => 'Sans suite',
        ],
        'label' => false
      ])
      ->add('save', SubmitType::class, [
        'attr' => ['class' => 'btn-primary w-25 mx-auto d-block'],
  ]);
    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TMsg::class,
        ]);
  }
}
