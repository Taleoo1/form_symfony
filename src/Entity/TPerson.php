<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\TPersonRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TPersonRepository")
 * @ApiResource(
 * attributes={
 *    "force_eager"=false,
 *    "normalization_context"={"groups"="call"},
 *    "denormalization_context"={"groups"="write"},
 *    "formats" = {"json"},
 *    "enable_max_depth"="true"
 *  }
 * )
 */
class TPerson
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Groups({"call", "write"})
     */
    private $FirstName;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Groups({"call", "write"})
     */
    private $LastName;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"call", "write"})
     */
    private $Phone;

    /**
     * @ORM\ManyToMany(targetEntity=TEmail::class, mappedBy="tPeople")
     * @Groups({"call", "write"})
     * @MaxDepth(1)
     */
    private $id_EmailPerson;

    public function __construct(string $FirstName, string $LastName, string $Phone)
    {   
        $this->FirstName = $FirstName;
        $this->LastName = $LastName;
        $this->Phone = $Phone;
        $this->id_EmailPerson = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->FirstName;
    }

    public function setFirstName(?string $FirstName): self
    {
        $this->FirstName = $FirstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->LastName;
    }

    public function setLastName(?string $LastName): self
    {
        $this->LastName = $LastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->Phone;
    }

    public function setPhone(?string $Phone): self
    {
        $this->Phone = $Phone;

        return $this;
    }

    /**
     * @return Collection|TEmail[]
     */
    public function getIdEmailPerson(): Collection
    {
        return $this->id_EmailPerson;
    }

    public function addIdEmailPerson(TEmail $idEmailPerson): self
    {
        if (!$this->id_EmailPerson->contains($idEmailPerson)) {
            $this->id_EmailPerson[] = $idEmailPerson;
        }

        return $this;
    }

    public function removeIdEmailPerson(TEmail $idEmailPerson): self
    {
        $this->id_EmailPerson->removeElement($idEmailPerson);

        return $this;
    }
}
