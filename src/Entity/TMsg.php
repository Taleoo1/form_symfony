<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use App\Repository\TMsgRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TMsgRepository")
 * @ApiResource(
 *  attributes={
 *    "force_eager"=false,
 *    "normalization_context"={"groups"="call"},
 *    "denormalization_context"={"groups"="write"},
 *    "formats" = {"json"},
 *    "enable_max_depth"="true"
 *  }
 * )

 */
class TMsg
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 5,
     *      max = 255,
     *      minMessage = "Merci de rentrer un sujet d'au moins {{ limit }} caractères",
     *      maxMessage = "Merci de ne pas dépasser {{ limit }} caractères"
     * )
     * @Groups({"call", "write"})
     */
    private $Subject;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      min = 5,
     *      max = 999,
     *      minMessage = "Merci de rentrer un message d'au moins {{ limit }} caractères",
     *      maxMessage = "Merci de ne pas dépasser {{ limit }} caractères"
     * )
     * @Groups({"call", "write"})
     */
    private $msg;

    /**
     * @ORM\ManyToOne(targetEntity=TEmail::class, inversedBy="tMsgs")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"call", "write"})
     * @MaxDepth(1)
     */
    private $id_Emailmsg;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"call", "write"})
     */
    private $Date;

    /**
     * @ORM\Column(type="string", length=15)
     * @Groups({"call", "write"})
     */
    private $State;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->Subject;
    }

    public function setSubject(string $Subject): self
    {
        $this->Subject = $Subject;

        return $this;
    }

    public function getMsg(): ?string
    {
        return $this->msg;
    }

    public function setMsg(string $msg): self
    {
        $this->msg = $msg;

        return $this;
    }

    public function getIdEmailmsg(): ?TEmail
    {
        return $this->id_Emailmsg;
    }

    public function setIdEmailmsg(?TEmail $id_Emailmsg): self
    {
        $this->id_Emailmsg = $id_Emailmsg;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->State;
    }

    public function setState(string $State): self
    {
        $this->State = $State;

        return $this;
    }
}
